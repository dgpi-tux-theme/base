# DgPi TUX-theme basis

De basis voor het DgPi TUX-theme bestaande uit:

- De fundering voor de (sub)theme-componenten
- De herbruikbare (deel)componenten
- De generieke vormgeving (zoals stijlschema's, grafisch materiaal, ..)

Voor de ontwikkeling en gebruik van het DgPi TUX-theme worden onderstaande principes toegepast:

| `Principe` | Omschrijving | Operationalisatie |
|:-|:-|:-|
| `Inzichtelijkheid` | Gestandaardiseerde gebruikerservaring voor consistente raadpleging door meerdere groepen gebruikers | **(a)** Vaste indeling layout paginas; **(b)** Styling op basis van het [Materialize-raamwerk](ttps://github.com/Dogfalo/materialize) (UX-standaarsisatie op basis van Material Design). |
| `Responsiviteit` | Publicaties moeten op meerdere formaten en/of apparaten te raadplegen zijn | Minimaal pagina's met specifieke indeling voor *desktop, laptop en tablet* formaten |
| `Valideerbaarheid` | Komen tot een basis waarmee componenten van de gebruikerservaring met een geautomatiseerde aanpak kunnen worden getest | Layout-componenten op basis van *klasse-attribuut* herkenbaar maken |
