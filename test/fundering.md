---
weight: 1
title: Fundering
---

De *Layout* is het fundament voor het uitbrengen van Meta-data. Deze is voor voor mensleesbare publicaties (TUX) de structuur volgens de HTML-standaarden.

Zie de functionele beschrijving van de HUGO-implementatie in onderstaande tabel .

| Functie | HUGO-implementatie | Beschrijving |
|:-|:-|:-|
| Standaard layout | `layouts/_default` | Deze directorie bevat de templates die het standaard-concept van de *layout* definieren |
| Basis layout | `layouts/_default/baseof.html`| Dit template bevat de conceptuele indeling van de publiceren HTML-pagina's in de vorm van blokken die in de onfderstaande template-onderdelen worden ingevuld. Zie voor werking [Define the base template](https://gohugo.io/templates/base/#define-the-base-template) |
| Sectie layout | `layouts/_default/list.html` | Dit template bevat de standaard weergave voor een sectie. Een sectie is een content-directorie met meerdere pagina's. Zie voor werking [Lists of content in HUGO](https://gohugo.io/templates/lists/) |
| Pagina layout | `layouts/_default/single.html` | Dit template bevat de standaard weergave voor een enkele pagina. Zie voor werking [Single page templates](https://gohugo.io/templates/single-page-templates/) |
| Thuis-pagina layout | `layouts/home.html` | Dit template bevat de indeling voor de thuis-pagina van de HTML-publicatie. Zie voor werking [Homepage template](https://gohugo.io/templates/homepage/) |
| DgPi Componenten layout | `layouts/partials/dgpi` | Deze directorie bevat alle *componenten* waarmee de DgPi-templates worden opgebouwd. Zie voor werking [Partial templates](https://gohugo.io/templates/partials/) en [Componenten](../componenten) (voor details per DgPi-component) |

> **Directorie-structuur HUGO-implementatie Fundering**

```goat
  +--------+     +--------+
  |layouts +-+---+_default|
  +--------+ |   +----+---+
             |        +-- baseof.html
             |        +-- list.html
             |        +-- single.html
             |
             +--- home.html
             |
             |   +----------+  +------+
             +---+ partials +--+ dgpi |
                 +----------+  +------+
                 
```