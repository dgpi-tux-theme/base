---
weight: 2
title: Componenten
---

De `layouts/partials/dgpi`-directorie bevat de *componenten* voor de DgPi-templates. In onderstaande tabel wordt de HUGO-implamentatie van deze *componenten* beschreven.

| HUGO-implementatie | Reden opdeling | Beschrijving |
|:-|:-|:-|
| `html-head.html` | **V** | Meta-data, generieke vormgeving etc. |
| `html-head-title` | **V / A / H** | Ophalen en/of generatie site-titel |
| `styles.html` | **V** | Laden van generieke vormgeving zoals material-stijlen, huisstijl (inclusief iconen, lettertypes, ...). Zie verder [Vormgeving](../vormgeving) |
| `hp-header.html` | **V / A / H** | Houder van alle componenten die de bovenkant van een (thuis-)pagina weergeven |
| `hp-000.html` | **T / A / H** | Basis weergave van de bovenkant van een (thuis-)pagina |
| `hp-main` | **V / A** | Houder van alle componenten de de inhoud van een thuis-pagina weergeven |
| `hp-main-list` | **V / A / H** | Houder van alle componenten die de inhoud van een sectie-pagina weergeven |
| `hp-main-single` | **V / A / H** | Houder van alle componenten die de inhoud van een enkele pagina weergeven |

>| Leganda - Reden opdeling |
|:-:|:-|
| **T** | Testbaarheid van een functioneel en/of technisch onderdeel |
| **V** | Verantwoordelijkheid voor bepaalde aspecten en/of afhandelingen |
| **A** | Aanpasbaarheid zoals maatwerk-opties e.d. |
| **H** | Herbruikbaarheid |

